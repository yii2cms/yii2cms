<?php
//Yii::setAlias('@test', realpath(dirname(__FILE__).'/../../app/test'));
//die(Yii::getAlias('@app'));


return [
	'id' => 'app-common',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'basePath' => dirname(__FILE__).'/../../app',
    'language' => 'ru-RU',
    //'bootstrap' => ['log'],
    // 'controllerPath' => '@app/controllers',
    //'controllerNamespace' => 'app\controllers',
    // 'controllerMap' => [
    //             'site' => 'app\controllers\SiteController'
    //     ],
    'aliases' => [
        '@pub' => '@app/web',
        //'@uploads' => '@app/web/uploads',
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
        ],
        
        'news' => [
            'class' => 'app\modules\news\News',
        ],

        'user' => [
            'class' => 'app\modules\user\User',
        ],

        'shop' => [
            'class' => 'app\modules\shop\Shop',
        ],
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            // 'enableStrictParsing' => true,
            'rules' => [
                'gii' => 'gii',
                'users' => 'users/index',
                'users/' => 'users/view',
                'login' => 'site/login',
            	// 'site/index'=>'site/index',
                //'site/contact'=>'site/contact',

            	'<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
            ],
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'LtZwZbAL1Z0syBA-QaGdbP0xHQToujhn',
        ],

        'assetManager' => [
            'basePath' => '@webroot/app/web/assets',
            'baseUrl' => '/assets'
        ],

        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
        ],

        
    ],
];

