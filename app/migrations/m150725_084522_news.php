<?php

use yii\db\Schema;
use yii\db\Migration;

class m150725_084522_news extends Migration
{

    public $db;
 
    /*public function __construct(DB $db) {
        $this->db = $db;
    }
*/

    public function up()
    {
        //echo "Щас создам таблицу новостей, 6 сек.";

        $this->execute('CREATE TABLE IF NOT EXISTS {{%news}} (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `title` varchar(300) NOT NULL,
                              `short` varchar(500) NOT NULL,
                              `full` text NOT NULL,
                              `image` varchar(255) NOT NULL,
                              `date` int(11) NOT NULL,
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;');

        //echo "Заполняю таблицу новостей данными.";

        $this->execute("INSERT INTO {{%news}} (`id`, `title`, `short`, `full`, `image`, `date`) VALUES
                            (1, 'dd', '<p>dd</p>\r\n', '<p>ddd</p>\r\n', '92eb7953594d106c5e43e70b1bb1ebfd.jpeg', 1421265600),
                            (3, 'wete', '<p>ert</p>\r\n', '<p>ert</p>\r\n', 'ed4f315426f44c168ea51209b6bac8ab.jpeg', 1436040000);");
        /*$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING . ' NOT NULL',

            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);*/
        

    }

    public function down()
    {
        //echo "Удаление таблицы новостей.\n";

        $this->dropTable('{{%news}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
