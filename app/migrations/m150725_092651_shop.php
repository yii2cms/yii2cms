<?php

use yii\db\Schema;
use yii\db\Migration;

class m150725_092651_shop extends Migration
{
    public function up()
    {

        $this->execute("CREATE TABLE IF NOT EXISTS {{%shop_catalog}} (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) NOT NULL,
                          `description` text NOT NULL,
                          `parent_id` int(11) NOT NULL,
                          `alias` varchar(255) NOT NULL,
                          `uri` text NOT NULL,
                          `created` int(11) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;");

        $this->execute("INSERT INTO {{%shop_catalog}} (`id`, `name`, `description`, `parent_id`, `alias`, `uri`, `created`) VALUES
                        (1, 'Бытовая техника', '', 0, 'bytovaya-tekhnika', '/bytovaya-tekhnika', 1437586477),
                        (2, 'Планшеты', '', 1, 'planshety', '/bytovaya-tekhnika/planshety', 1437587940),
                        (3, 'Asus', '', 2, 'asus', '/bytovaya-tekhnika/planshety/asus', 1437586527),
                        (4, 'Apple', '', 2, 'apple', '/bytovaya-tekhnika/planshety/apple', 1437518023),
                        (5, 'Samsung', '', 2, 'samsung', '/bytovaya-tekhnika/planshety/samsung', 1437523859),
                        (6, '16-ти гигабайтные', '', 5, '16-ti-gigabaitnye', '/bytovaya-tekhnika/planshety/samsung/16-ti-gigabaitnye', 1437515939),
                        (12, '16-ти гигабайтные', '', 3, '16-ti-gigabaitnye-2', '/bytovaya-tekhnika/planshety/asus/16-ti-gigabaitnye-2', 1437527514),
                        (13, 'Расходные материалы', '', 0, 'raskhodnye-materialy', '/raskhodnye-materialy', 1437580410),
                        (14, 'Салфетки для монитора', '', 13, 'salfetki-dlya-monitora', '/raskhodnye-materialy/salfetki-dlya-monitora', 1437580434),
                        (15, 'Краски для принтеров', '', 13, 'kraski-dlya-printerov', '/raskhodnye-materialy/kraski-dlya-printerov', 1437580469),
                        (16, '123', '<p>123</p>\r\n', 12, '123', '/bytovaya-tekhnika/planshety/asus/16-ti-gigabaitnye-2/123', 1437773386);");

        
        

        $this->execute("CREATE TABLE IF NOT EXISTS {{%shop_item}} (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(300) NOT NULL,
                          `description` text NOT NULL,
                          `price` float NOT NULL,
                          `catalog_id` int(11) NOT NULL,
                          `alias` varchar(350) NOT NULL,
                          `uri` text NOT NULL,
                          `created` int(11) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;");

        $this->execute("INSERT INTO {{%shop_item}} (`id`, `name`, `description`, `price`, `catalog_id`, `alias`, `uri`, `created`) VALUES
                        (1, 'were', '<p>wer</p>\r\n', 2.3, 12, 'were-i-1', '/bytovaya-tekhnika/planshety/asus/16-ti-gigabaitnye-2/were-i-1', 1437773367),
                        (2, 'кпк', '<p>укп</p>\r\n', 23, 12, 'kpk-i-2', '/bytovaya-tekhnika/planshety/asus/16-ti-gigabaitnye-2/kpk-i-2', 1437773489),
                        (3, 'вап', '<p>вап</p>\r\n', 3, 12, 'vap-i-3', '/bytovaya-tekhnika/planshety/asus/16-ti-gigabaitnye-2/vap-i-3', 1437773571);");
    

        $this->execute("CREATE TABLE IF NOT EXISTS {{%shop_item_image}} (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `item_id` int(11) NOT NULL,
                          `image` varchar(255) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS {{%user}} (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                          `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                          `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                          `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                          `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                          `status` smallint(6) NOT NULL DEFAULT '10',
                          `created_at` int(11) NOT NULL,
                          `updated_at` int(11) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;");

        $this->execute("INSERT INTO {{%user}} (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
                        (1, 'Rashid', 'WxYRU9HXVbRao8GBIRQgzkwN0U3uajpX', '$2y$13$gOO.HSXeze4l2AJ8/rKRyOl7xX44bVcIabb1gqEXd6c1YEQ.RMwd2', 'iCZYZ4w87beDMHlfgcU4C3ZtZ6LdS8WR_1434903063', 'web-technologies@yandex.ru', 10, 1434897964, 1434903063),
                        (2, 'Rashid1', '', '$2y$13$Lxsocq/bfZR8DJeLWbIiiOaA1cJgF4TAZJL2mhj0r0Mcj/yaol0yi', NULL, 'raha1987@mail.ru', 10, 1436964864, 1437082963),
                        (3, 'Rashid2', 'Fpcy9VPx70P6yUZun4MFjnNOpk2imbfy', '$2y$13$HcoBJM9cVwHe9DpqooDuH.2p/XOSSub.sWKElrp4XN0aK/WSP.PI6', NULL, 'rashid@rashid.ru', 10, 1436990178, 1436990178);");

    }

    public function down()
    {
        //echo "m150725_092651_shop cannot be reverted.\n";

        $this->dropTable('{{%shop_catalog}}');
        $this->dropTable('{{%shop_item}}');
        $this->dropTable('{{%shop_item_image}}');
        $this->dropTable('{{%user}}');

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
