<?php
namespace app\components;

use yii\web\Controller;
use Yii;

class BackController extends Controller
{
	public $layout = '/admin/main';

	protected function Test(){
		echo "Test Back";
	}

	public function init(){
		
		if (!Yii::$app->user->can('admin')) {
            return $this->redirect('/');
        }
	}
}
