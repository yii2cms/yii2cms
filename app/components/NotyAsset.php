<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NotyAsset extends AssetBundle
{
    public $sourcePath = '@bower/noty/js/noty/packaged';
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $css = [
        //'css/animate.min.css',
    ];
    public $js = [
        'jquery.noty.packaged.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\components\AnimateCssAsset',
    ];
}
