<?
namespace app\components;

use yii\web\UploadedFile;

/**
* 
*/
class IMG
{

	public static function save($model, $imageFieldInTable ,$form_image, $uploadDirField, $flagDelete){

		$uploadDir = \Yii::getAlias('@pub').'/'.$model->$uploadDirField;
		$fullPathToFile = $uploadDir.'/'.$model->$imageFieldInTable;
		$urlPathToFile = $model->$uploadDirField.'/'.$model->$imageFieldInTable;
			
		$delImgFunc = function($model, $imageFieldInTable, $urlPathToFile){
							$model->$imageFieldInTable = '';
							self::deleteImg($urlPathToFile); //кидаем URL путь, а не физический, так сделал чтобы можно было применять функцию вне этой функции(родительской)

						};

		if(isset($_REQUEST[$flagDelete])){
			$delImgFunc($model, $imageFieldInTable, $urlPathToFile);
		}

		$className =  $model->formName();
		$tmpFile = $_FILES[$className]['tmp_name'][$form_image];

		if(!is_file($tmpFile))
			return false;

		
		if(!is_dir($uploadDir)){
			mkdir($uploadDir, 0775);
		}

		$filename = md5(time().rand()).self::getExt($tmpFile);
		$fullSavePath = $uploadDir.'/'.$filename;

		//die($fullSavePath);

		/*$imagine  = \Yii\Imagine\Image::getImagine()->open($tmpFile);
		$size = $imagine
		
		// $transformation = new \Imagine\Filter\Transformation();

		// $transformation->apply($imagine)->resize(new \Imagine\Image\Box(1000, 1000))
		//     ->save($fullSavePath);*/



		/*\yii\imagine\Image::$driver = 'gd2';
		\yii\imagine\Image::thumbnail($tmpFile,1000,1000)
        ->save($fullSavePath, ['quality' => 100]);*/

        if(!self::_do_resize($tmpFile, $fullSavePath, 1000, 1000))
        	return false;

        $delImgFunc($model, $imageFieldInTable, $urlPathToFile);
        $model->$imageFieldInTable = $filename;

		/*$model->$form_image = UploadedFile::getInstance($model, $form_image);

        if ($model->$form_image) {                
            $model->$form_image->saveAs($uploadDir . '/' . $model->$form_image->baseName . '.' . $model->$form_image->extension);
        }*/
		
		//echo "Каталог закачки $uploadDir<BR>Свойство: {$model->$form_image}";
	}

	public static function deleteImg($url){

		$fullPath = \Yii::getAlias('@pub').'/'.$url;
		$uploadDir = dirname(\Yii::getAlias('@pub').'/'.$url);
		$filename = basename($fullPath);
		
		self::clearCacheForImage($uploadDir, $filename);

		@unlink($fullPath);

	}

	public static function clearCacheForImage($fromDir, $filename){
		$foldersCache = scandir($fromDir);
		foreach ($foldersCache as $name) {
			if($name == '.' || $name == '..')
				continue;

			$fullPathCacheFile = $fromDir.'/'.$name.'/'.$name.'_'.$filename;

			if(is_file($fullPathCacheFile)){
				
				@unlink($fullPathCacheFile);
			}
		}
	}

	public static function thumb($url, $width, $height){
	
		$fullPath = \Yii::getAlias('@pub').'/'.$url;

		if(!is_file($fullPath)) // Если нет оригинального файл возвращаем просто пустую строку
			return '';

		$uploadDir = dirname(\Yii::getAlias('@pub').'/'.$url);
		$filename = basename($fullPath);
		$uploadDirThumb = $uploadDir.'/'.$width.'x'.$height;
		$uploadDirThumbUrl = dirname($url).'/'.$width.'x'.$height;
		$filenameThumb = $width.'x'.$height.'_'.$filename;
		$fullPathThumb = $uploadDirThumb.'/'.$filenameThumb;
		$fullPathThumbUrl = '/'.$uploadDirThumbUrl.'/'.$filenameThumb;

		if(!is_dir($uploadDirThumb)){
			mkdir($uploadDirThumb, 0775);
		}

		// if(!is_file($fullPath))
		// 	throw new \Exception("Не найден файл: $fullPath! Поле таблицы имеет ");
			

		//die($fullPathThumb);

		if(is_file($fullPathThumb))
			return $fullPathThumbUrl;

		if(self::_do_thumb($fullPath, $fullPathThumb, $width, $height))
        	return $fullPathThumbUrl;

	}

	public static function resize($url, $width, $height){
	
		$fullPath = \Yii::getAlias('@pub').'/'.$url;

		if(!is_file($fullPath)) // Если нет оригинального файл возвращаем просто пустую строку
			return '';

		$uploadDir = dirname(\Yii::getAlias('@pub').'/'.$url);
		$filename = basename($fullPath);
		$uploadDirThumb = $uploadDir.'/'.$width.'x'.$height;
		$uploadDirThumbUrl = dirname($url).'/'.$width.'x'.$height;
		$filenameThumb = $width.'x'.$height.'_'.$filename;
		$fullPathThumb = $uploadDirThumb.'/'.$filenameThumb;
		$fullPathThumbUrl = '/'.$uploadDirThumbUrl.'/'.$filenameThumb;

		if(!is_dir($uploadDirThumb)){
			mkdir($uploadDirThumb, 0775);
		}

		if(is_file($fullPathThumb))
			return $fullPathThumbUrl;

		if(self::_do_resize($fullPath, $fullPathThumb, $width, $height))
			return $fullPathThumbUrl;
		

	}

	public static function _do_resize($source, $dest, $width, $height){
		
		\yii\imagine\Image::$driver = 'gd2';
		
		$img = \yii\imagine\Image::getImagine()->open($source);
		$size = $img->getSize();
		$oWidth = $size->getWidth();
		$oHeight = $size->getHeight();

		if($oWidth<$width && $oHeight<$height)
			if(copy($source, $dest))
				return true;

		$ratio = $oWidth/$oHeight;
		if(($width+$height) == 0){
			$width = $oWidth;
			$height = $oHeight;
		} elseif($width == 0){
			$width = round($height*$ratio);
		} elseif ($height==0) {
			$height = round($width/$ratio);
		} elseif($oWidth>$oHeight){
			$height = round($width/$ratio);
		} elseif($oWidth<$oHeight){
			$width = round($height*$ratio);
		}

		$box = new \Imagine\Image\Box($width, $height);
		if($img->resize($box)->save($dest, ['quality' => 100]))
			return true;

		/*if(\yii\imagine\Image::thumbnail($fullPath,$width,$width)->save($fullPathThumb, ['quality' => 100]))
        	return $fullPathThumbUrl;*/
	}

	public static function _do_thumb($source, $dest, $width, $height){
		\yii\imagine\Image::$driver = 'gd2';

		if(\yii\imagine\Image::thumbnail($source,$width,$height)->save($dest, ['quality' => 100]))
        	return true;
	}

	public static function getExt($filePath){
		if($imgType = @exif_imagetype($filePath))
			return image_type_to_extension($imgType, true);

	}
	
}