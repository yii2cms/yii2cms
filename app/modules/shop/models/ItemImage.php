<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_item_image}}".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $image
 */
class ItemImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_item_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'image'], 'required'],
            [['item_id'], 'integer'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'image' => 'Image',
        ];
    }
}
