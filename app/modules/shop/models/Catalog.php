<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_catalog}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $parent_id
 * @property string $alias
 * @property string $uri
 * @property integer $created
 */
class Catalog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_catalog}}';
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'alias',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general. 
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'uri', 'parent_id'], 'safe'],
            [['parent_id'],'default', 'value'=>0],
            [['parent_id', 'created'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'parent_id' => 'Родительский каталог',
            'alias' => 'Алиас',
            'uri' => 'Uri путь',
            'created' => 'Дата создания',
        ];
    }

    public function getChilds(){
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    public function getItems(){
        return $this->hasMany(Item::className(), ['catalog_id' => 'id']);
    }

    public function init(){
        if(isset($_GET['parent_id']) && $_GET['parent_id'] > 0)
            $this->parent_id = $_GET['parent_id'];
    }

    public function beforeSave($insert){
        
        if(parent::beforeSave($insert)){
            //$this->setAlias();
            $this->setCreatedTime();
            $this->setUri();
            return true;
        }
    }

    /*protected function setAlias(){
        $this->alias = \dosamigos\transliterator\TransliteratorHelper::process($this->name,'-','en');
    } */

    protected function setCreatedTime(){
        $this->created = time();
    }

    protected function setUri(){

        if($this->parent_id > 0){
            $uri = '';
            foreach ($this->chain as $item) {
                $uri .= '/'.$item->alias;
            }

            $this->uri = $uri;
        } else {
            $this->uri = '/'.$this->alias;
        }
    }

    protected function getChain(){
        $parent_id = $this->parent_id;
        if($this->alias)
            $chain[] = $this;

        while ($parent_id > 0) {
            $parent = self::findOne($parent_id);
            $chain[] = $parent;
            $parent_id = $parent->parent_id;
        }

        return array_reverse($chain);
    }

    public function getBreadcrumbsPart($options = ['activeLastItem' => true, 'hiddenLastItem'=>false]){

        $breadcrumbs = [];

        if($this->parent_id > 0){

            $chain = $this->chain;

            if(count($chain)){

                if($options['activeLastItem'])
                    $last = array_pop($chain);

                foreach ($chain as $i => $item) {

                    $breadcrumbs[] = [
                                        'label' => $item->name,
                                        'url' => ['catalog-admin/index','parent_id'=>$item->id]
                                     ];

                }

                if($options['activeLastItem'])
                    $breadcrumbs[] = $last->name;
            }

        } /*else {

            $breadcrumbs[] = ['label' => 'Каталог', 'url' => ['index']];;
        }*/

        if($options['hiddenLastItem'])
            array_pop($breadcrumbs); // скрыть(т.е. удалить из массива) последний элемент

        return $breadcrumbs;
    }

    /*protected function setAlias(){
        $this->alias = \dosamigos\transliterator\TransliteratorHelper::process($this->name,'-','en');
    }*/

    public static function getListCatalogs($current_id = 0){
        return self::listCatalogsTree(Catalog::find()->where('parent_id = 0')->all(), 0, $current_id);
    }

    public static function listCatalogsTree($catalogs, $vol = 0, $current_id){

        if(count($catalogs)){
            $list = [];
            foreach ($catalogs as $c) {

                if($c->id == $current_id)
                    continue;

                $list[$c->id] = str_repeat('-', $vol).$c->name;
                if(count($c->childs)){
                    $vol++;
                    $list += self::listCatalogsTree($c->childs, $vol, $current_id);
                    $vol--;
                }
            }

            return $list;
            
        }

    }

    public static function get(){
        return self::findOne($_GET['parent_id']);
    }
}
