<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_item}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property integer $catalog_id
 * @property string $alias
 * @property string $uri
 * @property integer $created
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_item}}';
    }

    public function init(){
        // if(Yii::$app->controller->module->id == "shop" && Yii::$app->controller->id == "catalog-admin")
            if(isset($_GET['parent_id']) && $_GET['parent_id'] > 0)
                $this->catalog_id = $_GET['parent_id'];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'alias',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general. 
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'price', 'catalog_id'], 'required'],
            [['description'], 'safe'],
            [['price'], 'filterPrice'],
            [['catalog_id'], 'integer'],
        ];
    }

    public function filterPrice($attribute, $params){
        $this->$attribute = str_replace(',', '.', $this->$attribute);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'price' => 'Цена',
            'catalog_id' => 'Каталог',
            'alias' => 'Алиас',
            'uri' => 'Полный путь',

        ];
    }

    public function beforeSave($insert){
        
        if(parent::beforeSave($insert)){
            $this->setCreatedTime();
            
            return true;
        }
    }

    public function afterSave($insert, $changedAttributes){
        $this->addStrToAlias();
        parent::afterSave($insert, $changedAttributes);
    }

    protected function addStrToAlias(){
        $this->alias = $this->alias.'-i-'.$this->id;
        $this->setUri(); 
        self::updateAll(['alias' => $this->alias, 'uri' => $this->uri], "id = $this->id");
    }

    protected function setCreatedTime(){
        $this->created = time();
    }

    protected function setUri(){
        $catalog = Catalog::findOne($this->catalog_id);
        $this->uri = $catalog->uri.'/'.$this->alias;
    }
}
