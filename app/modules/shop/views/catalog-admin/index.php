<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Каталог';
$this->params['breadcrumbs'][] = isset($_GET['parent_id'])?['label' => 'Каталог', 'url' => ['index']]:$this->title;
$this->params['breadcrumbs'] =  array_merge($this->params['breadcrumbs'], $breadcrumbsPart); 
?>
<div class="catalog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать каталог', ['create','parent_id'=>$_GET['parent_id']], ['class' => 'btn btn-success']) ?>
    </p>

    <br>

    <div class="catalogBlock">
        <h2>Категории</h2>
        <div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function($data){
                            return Html::a( $data->name, ['index','parent_id'=>$data->id],
                                        ['title' => 'Перейти в каталог']);
                        }
                    ],
                    'description:ntext',
                    'parent_id',
                    'alias',
                    'uri:ntext',
                    // 'created',

                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{update}{delete}',
                        'buttons' => [
                            'update'=>function ($url, $model) {
                                    return Html::a( '<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id, 'parent_id'=>$_GET['parent_id']],
                                        ['title' => 'Редактировать']);
                                },
                            'delete'=>function ($url, $model) {
                                    return Html::a( '<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id, 'parent_id'=>$_GET['parent_id']],
                                        ['title' => 'Удалить']);
                                }
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?if($currentCatalog && count($currentCatalog->items)){?>
        <br><br>
        <div class="itemBlock">
            <h2>Товары</h2>
            <div>
                <?= $this->render('/item-admin/_gridView')?>
            </div>
        </div>
    <?}?>

</div>
