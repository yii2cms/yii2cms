<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\modules\shop\models\Catalog;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Catalog */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="catalog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',

    ]) ?>
    
    <?= $form->field($model, 'parent_id')
        ->dropDownList(
            Catalog::getListCatalogs($model->id),           // Flat array ('id'=>'label')
            ['prompt'=>'Выберите каталог']    // options
        );
    ?>

    <?//= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
