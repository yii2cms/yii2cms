<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Item */

$this->title = 'Редактировать товар: ' . ' ' . $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'] = $breadcrumbsPart;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
