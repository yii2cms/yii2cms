<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\modules\shop\models\Catalog;
//namespace app\modules\shop\models;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',

    ]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'catalog_id')
        ->dropDownList(
            Catalog::getListCatalogs(),           // Flat array ('id'=>'label')
            ['prompt'=>'Выберите каталог']    // options
        );
    ?>

    <?//= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'uri')->textarea(['rows' => 6]) ?>

    <?//= $form->field($model, 'created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
