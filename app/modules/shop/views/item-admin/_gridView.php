<?
use yii\helpers\Html;
use app\modules\shop\models\Item;
use app\modules\shop\models\ItemSearch;

$searchModel = new ItemSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>

<p>
    <?= Html::a('Дабавить товар', ['item-admin/create', 'parent_id'=>$_GET['parent_id']], ['class' => 'btn btn-success']) ?>
</p>

<?= yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description:ntext',
            'price',
            'catalog_id',
            // 'alias',
            // 'uri:ntext',
            // 'created',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttons' => [
                    'update'=>function ($url, $model) {
                            return Html::a( '<span class="glyphicon glyphicon-pencil"></span>', ['item-admin/update', 'id' => $model->id, 'parent_id'=>$_GET['parent_id']],
                                ['title' => 'Редактировать']);
                        },
                    'delete'=>function ($url, $model) {
                            return Html::a( '<span class="glyphicon glyphicon-trash"></span>', ['item-admin/delete', 'id' => $model->id, 'parent_id'=>$_GET['parent_id']],
                                ['title' => 'Удалить']);
                        }
                ]
            ],
        ],
    ]); ?>