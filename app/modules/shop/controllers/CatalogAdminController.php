<?php

namespace app\modules\shop\controllers;

use Yii;
use app\modules\shop\models\Catalog;
use app\modules\shop\models\CatalogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\BackController;

/**
 * CatalogAdminController implements the CRUD actions for Catalog model.
 */
class CatalogAdminController extends BackController
{
    public function behaviors()
    {
        return [
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * Lists all Catalog models.
     * @return mixed
     */
    public function actionIndex($parent_id = false)
    {
        $searchModel = new CatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $breadcrumbsPart = $searchModel->breadcrumbsPart;

        $currentCatalog = Catalog::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'breadcrumbsPart' => $breadcrumbsPart,
            'currentCatalog' => $currentCatalog,
        ]);
    }

    /**
     * Displays a single Catalog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Catalog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Catalog();
        $breadcrumbsPart = $model->getBreadcrumbsPart(['activeLastItem'=>false]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'parent_id' => $_GET['parent_id']]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'breadcrumbsPart' => $breadcrumbsPart,
            ]);
        }
    }

    /**
     * Updates an existing Catalog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $breadcrumbsPart = $model->getBreadcrumbsPart(['hiddenLastItem'=>true]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'parent_id' => $_GET['parent_id']]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'breadcrumbsPart' => $breadcrumbsPart,
            ]);
        }
    }

    /**
     * Deletes an existing Catalog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'parent_id' => $_GET['parent_id']]);;
    }


    /**
     * Finds the Catalog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Catalog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Catalog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
