<?php

namespace app\modules\shop\controllers;

use Yii;
use app\modules\shop\models\Item;
use app\modules\shop\models\ItemSearch;
use app\modules\shop\models\Catalog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\BackController;

/**
 * ItemAdminController implements the CRUD actions for Item model.
 */
class ItemAdminController extends BackController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);*/

        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,            
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();
        $catalog = Catalog::get();
        $breadcrumbsPart = $this->getBreadcrumbs($catalog);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Товар успешно добавлен!'); 
            return $this->goUrl();
        } else {
            return $this->render('create', [
                'model' => $model,
                'breadcrumbsPart' => $breadcrumbsPart,
            ]);
        }
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $catalog = Catalog::get();
        $breadcrumbsPart = $this->getBreadcrumbs($catalog);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Товар успешно обновлен!'); 
            return $this->goUrl();
        } else {
            return $this->render('update', [
                'model' => $model,
                'breadcrumbsPart' => $breadcrumbsPart,
            ]);
        }
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', 'Товар успешно удален!'); 
        return $this->goUrl();
    }


    public function goUrl(){
        if(isset($_GET['parent_id']))
            return $this->redirect(['catalog-admin/index', 'parent_id' => $_GET['parent_id']]);
        else
            return $this->redirect(['index']);
    }

    public function getBreadcrumbs($catalog){
        
        $breadcrumbsPart = [];
        if($catalog){
            $breadcrumbsPart[] = ['label' => 'Каталог', 'url' => ['catalog-admin/index']];
            $breadcrumbsPart = array_merge($breadcrumbsPart, $catalog->getBreadcrumbsPart(['activeLastItem'=>false]));
        } else {
            $breadcrumbsPart[] = ['label' => 'Товары', 'url' => ['index']];
        }

        return $breadcrumbsPart;

    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
