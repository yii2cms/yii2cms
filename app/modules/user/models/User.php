<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{

    public $form_password;
    public $actives = [10=>'Активен', 0=>'Не активен'];
    public $roles = ['registered'=>'Зарегистрированный', 'admin'=>'Администратор'];
    public $_role = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'status', 'role'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'email'], 'string', 'max' => 255],
            ['role','validateRole'],
            [['form_password'],'safe'],
        ];
    }

    public function validateRole($attribute, $params){
        if(!isset($this->roles[$this->$attribute]))
            $this->addError($attribute, 'Введите существующую роль');
    }

    public function beforeSave($insert){
        $this->setPassword();
        $this->setRoles();
        $this->setTime();
        return parent::beforeSave($insert);
    }

    public function setRoles(){
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($this->role);
        if($auth->revokeAll($this->id))
            $auth->assign($role, $this->id);
    }

    public function setTime(){
        if($this->isNewRecord){
            $this->created_at = time();
            $this->updated_at = $this->created_at;
        } else {
            $this->updated_at = time();
        }
    }

    public function setPassword()
    {
        if(trim($this->form_password)!="")
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->form_password);
    }

    public function setRole($value){
        $this->_role = $value;
    }

    public function getRole(){
        if($this->_role)
            return $this->_role;
        else {
            $roles = Yii::$app->authManager->getRolesByUser($this->id);
            if(count($roles))
                return key($roles);
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'auth_key' => 'Ключь аутентификации',
            'password_hash' => 'Хэш пароля',
            'password_reset_token' => 'Токен сброса пароля',
            'email' => 'Email',
            'form_password' => 'Пароль',
            'status' => 'Статус',
            'role' => 'Роль',
            'created_at' => 'Создан',
            'updated_at' => 'Редактирован',
        ];
    }
}
