<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'form_password')->PasswordInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'status')
        ->dropDownList(
            $model->actives,           // Flat array ('id'=>'label')
            ['prompt'=>'Выберите статус']    // options
        );
    ?>
    
    <?= $form->field($model, 'role')
        ->dropDownList(
            $model->roles,           // Flat array ('id'=>'label')
            ['prompt'=>'Выберите роль']/*,*/    // options
            /*['options' =>
                [                        
                  (key(Yii::$app->authManager->getRolesByUser($model->id))) => ['selected' => true]
                ]
            ]*/
        );
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Применить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
