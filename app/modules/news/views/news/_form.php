<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
// use dosamigos\fileinput\BootstrapFileInput;
/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?//= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'short')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'full')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',

    ]) ?>

    <?= $form->field($model, 'full')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',

    ]) ?>

    <div class="imageRow">
        <div>
            <img src="<?=$model->imgResize(300, 300);?>">
        </div>
        <?= $form->field($model, 'form_image')->fileInput() ?>
    </div>

    <?/*= $form->field($model, 'form_image')->widget(BootstrapFileInput::className(), [ 
            'options' => ['accept' => 'image/*', 'multiple' => false], 
            'clientOptions' => [ 'previewFileType' => 'text',
                                 'browseClass' => 'btn btn-success',
                                 'uploadClass' => 'btn btn-info',
                                 'removeClass' => 'btn btn-danger', 'removeIcon' => ' ' ] ]);*/?>


    <?
        // echo DatePicker::widget([
        //     'model' => $model,
        //     'attribute' => 'date',
        //     //'language' => 'ru',
        //     //'dateFormat' => 'yyyy-MM-dd',
        // ]);
    ?>
    <?= $form->field($model, 'form_date')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]) ?>
    <?//= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
