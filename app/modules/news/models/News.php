<?php

namespace app\modules\news\models;

use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $short
 * @property string $full
 * @property integer $date
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $uploadDir = 'uploads/news';
    // public $uploadDirUrl = 'uploads/news';
    public $form_image;

    public static function tableName()
    {
        return '{{%news}}';
    }

    public function getForm_date(){
        if(!$this->date)
            return date('d-m-Y');
        else
            return date('d-m-Y', $this->date);
    }

    public function setForm_date($value){
        $this->date = strtotime($value);
    }

    public function beforeSave($insert){

        $this->saveImage();
        return parent::beforeSave($insert);
    }

    protected function saveImage(){
        \app\components\IMG::save($this, 'image','form_image','uploadDir', 'delImgFlag');
    }

    public function getImgPath(){
        return $this->uploadDir.'/'.$this->image;
    }

    public function imgResize($width, $height){
        return \app\components\IMG::resize($this->imgPath, $width, $height);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short', 'full'], 'required'],
            [['form_date'], 'date', 'format'=>'dd-MM-yyyy'],
            [['full'], 'string'],
            [['title'], 'string', 'max' => 300],
            [['short'], 'string', 'max' => 500],
            [['form_image'], 'file', 'extensions' => 'jpg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'short' => 'Краткое описание',
            'full' => 'Полный текст',
            'date' => 'Дата',
            'form_date' => 'Дата',
            'image' => 'Изображение',
            'form_image' => 'Изображение',
        ];
    }
}
